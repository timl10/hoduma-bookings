<?php

namespace Hoduma\Hbookings\Site\Dispatcher;

defined('_JEXEC') or die;

class Dispatcher extends \FOF30\Dispatcher\Dispatcher
{
	public $defaultView = 'Ressources';
	
	public function onBeforeDispatch()
	{
		// Load common CSS JavaScript
		\JHtml::_('jquery.framework');
		$this->container->template->addCSS('media://com_hbookings/css/frontend.css', $this->container->mediaVersion);
	}
}