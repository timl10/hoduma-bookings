<?php
 
namespace Hoduma\Hbookings\Admin\Model;
 
defined('_JEXEC') or die;
 
use FOF30\Container\Container;
use FOF30\Model\DataModel;
use JFactory;
use JDate;

class Bookings extends DataModel
{
	public function __construct(Container $container, array $config = array())
	{
		parent::__construct($container, $config);
		$this->addBehaviour('Filters');
		$this->addBehaviour('RelationFilters');
		$this->addBehaviour('Access');
 
		$this->belongsTo('ressource');
	}
	
	protected function onBeforeBuildQuery(\JDatabaseQuery &$query)
	{
		$this->filterByDate($query);
	}
	
	protected function filterByDate(\JDatabaseQuery $query)
	{
		$db = $this->getDbo();
		
		$tableAlias = $this->getBehaviorParam('tableAlias', null);
		$tableAlias = !empty($tableAlias) ? ($db->qn($tableAlias) . '.') : '';
		
		$app = JFactory::getApplication();
		$month = $app->getUserState('month', null);
		$year = $app->getUserState('year', null);
		
		if($month && $year) {
			$query->where(
				'(MONTH(' . $tableAlias . $db->qn('startdate') . ') = ' . $db->q($month) .' OR MONTH(' . $tableAlias . $db->qn('enddate') . ') = ' . $db->q($month) . ')'
			);
			$query->where(
				'YEAR(' . $tableAlias . $db->qn('startdate') . ') = ' . $db->q($year)
			);
		}
	}
	
}