<?php
 
namespace Hoduma\Hbookings\Admin\Model;
 
defined('_JEXEC') or die;
 
use FOF30\Container\Container;
use FOF30\Model\DataModel;
use JFactory;

class Ressources extends DataModel
{
	public function __construct(Container $container, array $config = array())
	{
		parent::__construct($container, $config);
		$this->addBehaviour('Filters');
		$this->addBehaviour('RelationFilters');
 
		$this->hasMany('bookings');
	}
}