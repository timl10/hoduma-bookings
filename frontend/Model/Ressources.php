<?php
 
namespace Hoduma\Hbookings\Site\Model;
 
defined('_JEXEC') or die;

use FOF30\Container\Container;

/**
 * This model extends the back-end model, pulling it into the frontend without duplicating the code
 */
class Ressources extends \Hoduma\Hbookings\Admin\Model\Ressources
{
	public function __construct(Container $container, array $config = array())
	{
		parent::__construct($container, $config);
		$this->addBehaviour('Enabled');
	}
}