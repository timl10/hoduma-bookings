<?php

defined('_JEXEC') or die();

include_once JPATH_LIBRARIES . '/fof30/include.php';

use \FOF30\Inflector\Inflector;

function HbookingsBuildRoute(&$query)
{
	$segments = array();

	if (isset($query['view']))
	{
		$segments[] = $query['view'];
		unset($query['view']);
	}
	if (isset($query['id']))
	{
		$segments[] = $query['id'];
		unset($query['id']);
	};
	return $segments;
}

function HbookingsParseRoute($segments)
{
       $vars = array();
	   
	   $vars['view'] = $segments[0];
	   $vars['id'] = $segments[1];
	   
       return $vars;
}