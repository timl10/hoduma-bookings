<?php

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
JFormHelper::loadFieldClass('list');
 
class JFormFieldRessource extends JFormFieldList
{
	protected $type = 'Ressource';
 
	protected function getOptions()
	{
		$db    = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('hbookings_ressource_id, title');
		$query->from('#__hbookings_ressources');
		$db->setQuery((string) $query);
		$items = $db->loadObjectList();
		$options  = array();
 
		if ($items)
		{
			foreach ($items as $item)
			{
				$options[] = JHtml::_('select.option', $item->hbookings_ressource_id, $item->title);
			}
		}
 
		$options = array_merge(parent::getOptions(), $options);
 
		return $options;
	}
}