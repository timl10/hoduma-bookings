CREATE TABLE IF NOT EXISTS `#__hbookings_bookings` (
	`hbookings_booking_id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`hbookings_ressource_id` BIGINT(20) UNSIGNED NOT NULL,
	`startdate` DATE NOT NULL DEFAULT '0000-00-00',
	`enddate` DATE NOT NULL DEFAULT '0000-00-00',
	`salutation` TINYINT(1) NOT NULL DEFAULT '0',
	`name` VARCHAR(100) NOT NULL DEFAULT '',
	`prename` VARCHAR(100) NOT NULL DEFAULT '',
	`email` VARCHAR(100) NOT NULL DEFAULT '',
	`phone` VARCHAR(100) NOT NULL DEFAULT '',
	`description` mediumtext NOT NULL,
	`confirmation` TINYINT(1) NOT NULL DEFAULT '0',
	`enabled` TINYINT(1) NOT NULL DEFAULT '1',
	PRIMARY KEY (`hbookings_booking_id`)
) ENGINE=InnoDB DEFAULT COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `#__hbookings_ressources` (
	`hbookings_ressource_id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`title` VARCHAR(255) NOT NULL,
	`slug` VARCHAR(255) NOT NULL DEFAULT '',
	`location` TEXT NOT NULL DEFAULT '',
	`description` mediumtext NOT NULL,
	`access` INT(11) NOT NULL DEFAULT '0',
	`enabled` TINYINT(1) NOT NULL DEFAULT '1',
	`ordering` BIGINT(20) UNSIGNED NOT NULL,
	`created_on` DATETIME NOT NULL default '0000-00-00 00:00:00',
	`created_by` BIGINT(20) UNSIGNED NOT NULL DEFAULT '0',
	`modified_on` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`modified_by` BIGINT(20) UNSIGNED NOT NULL DEFAULT '0',
	`locked_on` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`locked_by` BIGINT(20) UNSIGNED NOT NULL DEFAULT '0',
	PRIMARY KEY (`hbookings_ressource_id`),
	KEY `idx_locked` (`locked_by`),
	KEY `idx_enabled` (`enabled`)
) ENGINE=InnoDB DEFAULT COLLATE = utf8_general_ci;