<?php

namespace Hoduma\Hbookings\Site\View\Ressources;

use Hoduma\Hbookings\Site\Model\Ressources;

defined('_JEXEC') or die;

class Html extends \FOF30\View\DataView\Html
{
	public $month = 0;
	public $year = 0;
	
	/**
	 * Executes before rendering the page for the Read task.
	 */
	protected function onBeforeRead()
	{
		$model = $this->getModel();
		
		$jinput = \JFactory::getApplication()->input;
		$this->month = $jinput->get('month', \JFactory::getDate()->format('m'), 'INT');
		$this->year = $jinput->get('year', \JFactory::getDate()->format('Y'), 'INT');
		
		$app = \JFactory::getApplication();
		$app->setUserState('month', $this->month);
		$app->setUserState('year', $this->year);
		
		parent::onBeforeRead();
	}
}